import axios from "axios";

const API_URL = apiUrl;

const AXIOS_HEADERS = {
  // 'withCredentials': true,
  // 'Access-Control-Allow-Credentials': 'true',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': '*',
  'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS',
  'X-Requested-With': 'XMLHttpRequest',
  'Accept': 'application/json',
  'Content-Type': 'apllication/json',
  'X-User-Jwt-Token' : '',
};

axios.defaults.baseURL = API_URL;
axios.defaults.headers = AXIOS_HEADERS;

axios.interceptors.request.use((config) => {
  // console.log(config);
  return config;
},  (error) => {
  return Promise.reject(error);
});

axios.interceptors.response.use((response) => {
  // console.log(response);
  return response;
},  (error) => {
  return Promise.reject(error);
});

class ApiService {

  response = {};
  result   = {};
  error    = null;
  status   = 0;

  token    = false;
  statusText = '';
  responseMessage = '';
  responseStatus  = false;

  constructor(apiUrl = null) {
     // this.apiUrl = url;
  }

  send(url, data = null, method = 'get', fn = null) {
    let response = null;
    if(!data) { // GET DELETE
      return axios({ method, url })
          .then((response) => {
              return this.responseHandler(response, fn);
          }).catch((error) => {
              this.catchErrorHandler(error)
          });
    }

    // POST PUT
    return axios({ method, url, data })
        .then((response) => {
           return this.responseHandler(response, fn);
        }).catch((error) => {
           this.catchErrorHandler(error)
        });
  }

  alert(message = null) {
    if(this.respondStatus) {
      alert('Успешно!' + this.responseMessage);
      return true;
    }
    alert('Не удалось выполнить!' + this.responseMessage);
  }

  responseHandler(response, fn) {

    this.response = response;
    const data    = response.data;
    if(!this.dataCheck(data, response))
      return false;

    const result  = data.result;

    if(fn)
      return fn(data);

    // console.log(data);

    this.responseMessage = data.message;
    this.respondStatus   = data.status

    return new Promise((resolve, reject) => {
       resolve(data);
    });

  }

  dataCheck(data, response = null) {
    if((typeof data) == 'string') {
      lg({ message: 'Ошибка в бизнес-логике сервера' , data });
      return false;
    }
    return true;
  }

  catchErrorHandler(error) {
    const message = 'Ошибка в axios catch';
    const errorData = error.response.data
    lg({ message , errorData, error, response: this.response });
  }

}

export default new ApiService();



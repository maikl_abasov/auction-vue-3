import axios from 'axios';

//const API_URL = 'https://jsonplaceholder.typicode.com';

// const API_URL   = 'http://192.168.1.67:856/api'; // laravel
// const API_URL   = 'http://localhost:8989';  // symfony
const API_URL   = apiUrl;  // symfony

const JWT_TOKEN_NAME = 'X-JWT-USER-TOKEN';

const HEADER_LIST = {
    // 'Accept': 'application/json',
    // 'Access-Control-Allow-Origin': '*',
    // 'Access-Control-Allow-Headers': '*',
    // 'Access-Control-Allow-Methods': 'GET,POST,PUT,DELETE,OPTIONS',
    // 'X-Requested-With': 'XMLHttpRequest',
    // 'Content-Type': 'multipart/form-data, apllication/json',
    // 'Content-Type': 'apllication/json',
    // 'X-User-Jwt-Token' : '',
}

const ApiInstance = axios.create({
    baseURL: API_URL,
    headers: HEADER_LIST,
    // headers: {"Content-Type": "application/json",},
});

const getToken = () => {
    return localStorage.getItem(JWT_TOKEN_NAME)
}

const removeToken = () => {
    localStorage.removeItem(JWT_TOKEN_NAME);
}

const setToken = (tokenValue) => {
    localStorage.setItem(JWT_TOKEN_NAME, tokenValue)
}

const jsonFormat = (data, key = 'parse') => {
    let result = null;
    if(key == 'parse') result = JSON.parse(data);
    else result = JSON.stringify(data);
    return result;
}

const refreshToken = async (err, originalConfig) => {
    if (originalConfig.url !== "/auth/signin" && err.response) {
        if (err.response.status === 401 && !originalConfig._retry) {
            originalConfig._retry = true;
            try {
                const rs = await ApiInstance.post("/auth/refreshtoken", {
                    refreshToken: TokenService.getLocalRefreshToken(),
                });
                const { accessToken } = rs.data;
                setToken(accessToken)
                return ApiInstance(originalConfig);
            } catch (_error) {
                return Promise.reject(_error);
            }
        }
    }
    return Promise.reject(err);
}

// setToken('SWD_UUI_MAIKL_TOKEN_JWT');

ApiInstance.interceptors.request.use(
    (config) => {
        const token = getToken();
        if (token) {
            config.headers[JWT_TOKEN_NAME] = token;
        }
        console.log('---interceptors.request.use---', config)
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

ApiInstance.interceptors.response.use(

    (result) => {
        console.log('---interceptors.response.use---', result)
        return result.data;
    },

    async (err) => {
        console.log('ERROR:interceptors.response.use');
        const originalConfig = err.config;
        return refreshToken(err, originalConfig)
    }
);

// --- Формируем api.url ---
//___ /user/info/{user_role}/{user_id} __шаблон url
//___ args['admin',34]                 __массив аргументов url
//___ /user/info/admin/34              __конечный результат

const urlFormat = (url, args) => {
    let urlList = url.split('/');
    let ch = 0;
    for(let i in urlList) {
        let value = urlList[i];
        if(value.indexOf('{') !== -1) {
            urlList[i] = args[ch];
            ch++;
        }
    }
    let newUrl = urlList.join('/');
    return newUrl;
}

const send = (param, args = null, data = null) => {
    let { method, url } = param;
    if(args && args.length) url = urlFormat(url, args);
    if(data) return ApiInstance[method](url, data);
    else     return ApiInstance[method](url);
}

const ApiClient = {
    api: ApiInstance,
    getToken,
    setToken,
    removeToken,
    send,
    JWT_TOKEN_NAME,
}

export default ApiClient;
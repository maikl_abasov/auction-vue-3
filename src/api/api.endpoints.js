import apiClient from '@/api/api.client';

let args = null;
let data = null;

const API_ENDPOINTS = {

    // Создать новый раунд аукциона
    addNewRound: (postData) => {
        const param = { url: '/admin/round-auction/create', method: 'post' };
        return apiClient.send(param, args, postData);
    },

    // Получить все раунды
    getAllRounds: (args) => {
        const param = { url: '/admin/round-auction/all-rounds/{auction_type}/{limit}', method: 'get' };
        return apiClient.send(param, args);
    },

    // Удалить раунд
    deleteRound: (roundId) => {
        const param = { url: '/admin/round-auction/delete/{round_id}', method: 'get' };
        return apiClient.send(param, [roundId]);
    },

    // Получить 1 раунд по id
    getRound: (roundId) => {
        const param = { url: '/admin/round-auction/item/{round_id}', method: 'get' };
        return apiClient.send(param, [roundId]);
    },

    // Получить активные раунды
    getActiveRounds: (auctionType) => {
        const param = { url: '/auction/active-rounds/list/{aution_type}', method: 'get' };
        return apiClient.send(param, [auctionType]);
    },

    // Добавить новые лоты
    addNewLots: (postData) => {
        const param = { url:  '/admin/lot-auction/add-lots', method: 'post' };
        return apiClient.send(param, args, postData);
    },

    // Получить активный аукцион
    getActiveAuction: (postData, roundId) => {
        const param = { url: '/auction/active-round-auction/lots/{round_id}', method: 'post' };
        return apiClient.send(param, [roundId], postData);
    },

    // Получить количество лотов активного аукциона
    getActiveAuctionCount: (postData, roundId) => {
        const param = { url: '/auction/active-round-auction/lot-counts/{round_id}', method: 'post' };
        return apiClient.send(param, [roundId], postData);
    },

    // Получить марки, модели авто, города ...
    getDataForSearchForm: (aution_type) => {
        const param = { url: '/auction/search-form/get-form-param/{aution_type}', method: 'get' };
        return apiClient.send(param, [aution_type]);
    },

    // Сделать ставку
    setNewBid: (postData) => {
        const param = { url: '/auction/bid/set-bid', method: 'post' };
        return apiClient.send(param, args, postData);
    },

    // Купить сейчас
    buyNowBid: (postData) => {
        const param = { url: '/auction/bid/set-buy-now', method: 'post' };
        return apiClient.send(param, args, postData);
    },

    // Получить ставки по лоту
    getBidsForLot: (lotId) => {
        const param = { url: '/auction/lot/bids-list/{lot_id}', method: 'get' };
        return apiClient.send(param, [lotId]);
    },

    // Получить лот
    getLot: (lotId) => {
        const param = { url: '/auction/lot/item/{lot_id}/id', method: 'get' };
        return apiClient.send(param, [lotId]);
    },


    userRegister: (user) => {
        const sendParam = { method: 'post', url: '/users/register' };
        return apiClient.send(sendParam, null, user);
    },

    getUsers: (param = null) => {
        const sendParam = { method: 'get', url: '/users/list' };
        return apiClient.send(sendParam, args, data);
    },

    getUserById: (userId) => {
        const sendParam = { method: 'get', url: '/users/user-by-id/{user_id}' };
        return apiClient.send(sendParam, [userId], data);
    },

    login: (auth) => {
        const sendParam = { method: 'post', url: '/users/login' };
        return apiClient.send(sendParam, null, auth);
    },

    // Получить профили пользователя
    getUserProfiles: (userId) => {
        const sendParam = { url: '/profiles/list-by/user_id/{value}', method: 'get'};
        return apiClient.send(sendParam, [userId], {});
    },

    // Получить 1 профиль
    getProfile: (profileId) => {
        const sendParam = { url: '/profiles/profile/{profile_id}', method: 'get'};
        return apiClient.send(sendParam, [userId], {});
    },

    // Установить активный профиль
    setActiveProfile: (profileId) => {
        const sendParam = { url: '/profiles/set-active/{profile_id}', method: 'get'};
        return apiClient.send(sendParam, [profileId]);
    },

    // Создать профиль
    createProfile: (data) => {
        const sendParam = { url: '/profiles/create', method: 'post'};
        return apiClient.send(sendParam, null, data);
    },

    // Изменить профиль
    updateProfile: (profileId, data) => {
        const sendParam = { url: '/profiles/update/{profile_id}', method: 'put'};
        return apiClient.send(sendParam, [profileId], data);
    },

    ///////////////////////////////////////
    ////////////////////
    ///////////////////

    getToken: () => {
        return apiClient.getToken();
    },

    removeToken: () => {
        return apiClient.removeToken();
    },

    setToken: (tokenValue) => {
        return apiClient.setToken(tokenValue);
    },

}

export default API_ENDPOINTS;
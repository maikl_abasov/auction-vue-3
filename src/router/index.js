import { createRouter, createWebHashHistory } from 'vue-router'

import Main from '@/pages/Site/Main';
// import Register from '@/pages/Site/Register'
import Auth from '@/pages/Site/Auth'
import Auction from '@/pages/Site/Auction'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Main
  },

  {
    path: '/user/auth/:action',
    name: 'user-auth-action',
    component: Auth,
  },

  {
    path: '/auction',
    name: 'auction',
    component: Auction
  },

  {
    path: '/admin-panel',
    name: 'admin-panel',
    component: () => import('@/pages/Admin/AdminPanel')
  },

  {
    path: '/personal-cabinet',
    name: 'personal-cabinet',
    component: () => import('@/pages/Site/PersonalCabinet')
  },

  // {
  //   path: '/auth/login',
  //   name: 'auth-login',
  //   component: () => import('@/pages/Auth.vue')
  // },

  // {
  //   path: '/auth/register',
  //   name: 'auth-register',
  //   component: () => import('@/pages/UserRegister.vue')
  // },
  //

]

const router = createRouter({
  history: createWebHashHistory(),
  // linkActiveClass: "top-menu-active",
  routes
})

export default router

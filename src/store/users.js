import { defineStore } from 'pinia';

import apiClient from '@/api/api.client';
import apiService from '@/api/api.endpoints';

export const useUsersStore = defineStore({
    id: 'users',
    state: () => ({
        token: apiClient.getToken(),
        refreshToken: '',
        storeName: 'user',
        users: {},
        user : JSON.parse(localStorage.getItem('user')),
        profiles: [],
        profile: {},
        user_id: 0,
        profile_id: 0,
    }),

    getters: {

        loadUserLocalStore: (state) => {
            let user = localStorage.getItem(this.storeName);
            if(!user) return false;
            state.user = JSON.parse(user);
            return state.user
        },

        loadToken: (state) => {
            let token = apiClient.getToken();
            if(token) state.token = token
            return state.token;
        },
    },

    actions: {

        setUserLocalStore(user) {
            localStorage.setItem(this.storeName, JSON.stringify(user));
        },

        register(user, fn = null) {
            return apiService.userRegister(user);
        },

        loadLocalStoreInfo() {
            let userLocal = localStorage.getItem(this.storeName);
            if(!userLocal) return false;
            this.user = JSON.parse(userLocal);
            this.user_id = this.user.id;
            if(this.user.profile) {
                this.profile = this.user.profile
                this.profile_id = this.user.profile_id
            }
        },

        async login(auth) {
            const response = await apiService.login(auth);
            if(response.result) {
                const result = response.result;
                if(result.token) {
                    this.user = result.user;
                    this.token = result.token;
                    this.refreshToken = result.refresh_token;
                    this.user_id = this.user.id;
                    if(this.user.profile) {
                        this.profile = this.user.profile;
                        this.profile_id = this.user.profile.id;
                    }
                    this.setUserLocalStore(result.user);
                    apiClient.setToken(result.token);
                }
            }
            return response;
        },

        logout() {
            this.user_id = 0;
            this.profile_id = 0;
            this.profile = {};
            this.user = {};
            this.token = null;
            apiClient.removeToken();
            localStorage.removeItem(this.storeName);
        },

        getUsers(fn = null, param =null) {
            apiService.getUsers(param).then(response => {
                if(fn) return fn(response);
                this.users = response
            })
        },

        getUserById(userId) {
            apiService.getUserById(userId).then(response => {
                console.log(response);
            })
        },

        getUserProfiles(userId, fn = null) {
            apiService.getUserProfiles(userId).then(response => {
                this.profiles = response
                if(fn) fn(response);
            })
        },

        getProfile(profileId, fn = null) {
            apiService.getProfile(profileId).then(response => {
                this.profile = response
                if(fn) fn(response);
            })
        },

        setActiveProfile(profileId) {
            apiService.setActiveProfile(profileId).then(response => {

                if(!response.result) {
                    alert('Не удалось установить профиль');
                    return false
                }

                this.getUserProfiles(this.user_id, (profiles) => {
                    this.profiles = profiles;
                    for(let i in profiles) {
                        let profile   = profiles[i];
                        if(profileId != profile.id) continue;
                        this.profile = profile
                        this.user.profile = profile
                        this.user.profile_id = profile.id
                        this.profile_id   = profile.id
                        this.setUserLocalStore(this.user)
                        return true;
                    }
                })
            })
        },

        createProfile(data, fn = null) {
            apiService.createProfile(data).then(response => {
                this.getUserProfiles(this.user_id)
                if(fn) fn(response);
            })
        },

        updateProfile(id, data, fn = null) {
            apiService.updateProfile(id, data).then(response => {
                this.getUserProfiles(this.user_id)
                if(fn) fn(response);
            })
        },

        // async delete(id) {
        //
        //     // this.users.find(x => x.id === id).isDeleting = true;
        //     // await fetchWrapper.delete(`${baseUrl}/${id}`);
        //     // this.users = this.users.filter(x => x.id !== id);
        //     // const authStore = useAuthStore();
        //     // if (id === authStore.user.id) {
        //     //     authStore.logout();
        //     // }
        //
        // },

    }
});

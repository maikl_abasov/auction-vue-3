import {createStore} from 'vuex'

import User from '@/store/modules/User'
import Auction from '@/store/modules/Auction'
import AdminPanel from '@/store/modules/AdminPanel'

export default createStore({

    modules: {
        User,
        Auction,
        AdminPanel,
    },

    state: {
        alertTimer: 5000,
        alertTitle: '',
        alertMessage: 'Запрос выполнен успешно',
        alertType: 'green',
        alertStatus: false,
    },

    mutations: {

        setAlertMessage(state, data) {
            state.alertMessage = data
        },

        setAlertStatus(state, data) {
            state.alertStatus = data
        },

        setAlertType(state, data) {
            state.alertType = data
        },

        setAlertTitle(state, data) {
            state.alertTitle = data
        },

        setAlertTimer(state, data) {
            state.alertTimer = data
        },
    },

    actions: {
    },

    //////////////////
    getters: {

        getAlertMessage(state) {
            return state.alertMessage;
        },

        getAlertStatus(state) {
            let timer = state.alertTimer;
            setTimeout(() => {
                state.alertStatus = false
            }, timer);
            return state.alertStatus;
        },

        getAlertType(state) {
            return state.alertType;
        },

        getAlertTitle(state) {
            return state.alertTitle;
        },
    },

});


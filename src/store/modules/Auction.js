// import httpClient from "@/api/api.service";
import Api from "@/api/api.endpoints";

const handle = {
    errorParse: (err) => {
        let request  = err.request;
        let response = request.response;
        let data = JSON.parse(response);
        let code = request.status;
        return { code, data, response, request }
    },

    saveParse: (response) => {
        let result  = response.result;
        let error   = response.error;
        let save    = (result.save) ? result.save : false;
        return { save, result, error }
    }
}

const Auction = {

    state: {

        activeRounds : [],
        activeAuction: [],
        allRoundsList: [],
        lotBids: [],
        lotItem: {},
        activeAuctionCount: 0,
        currentRoundId: 0,
        lotsCount: 0,
        newBidConfig: {
            bid: null,
            lot: null,
            fn: null,
            status: false,
        },

        searchForm : {},

        auctionTypes : {
            1 : 'Открытые торги',
            2 : 'Закрытые торги',
            3 : 'Онлайн торги',
            4 : 'Экспресс торги',
        },

        lotStatuses : {
            0 : 'Черновик',
            1 : 'Выставить на торгах',
            2 : 'Снять с торгов',
            3 : 'Торги закончены',
            4 : 'В архив',
        },

        lotBidsState: false,

        newBidResponseEvent: {
           status: false,
           lot: {},
           bid: {}
        },

        responseInfo: {
            errors: [],
            code: 0,
            message: '',
            status: false,
            action: '',
            data: [],
        },

    },

    mutations: {

        setActiveRounds(state, data) {
            state.activeRounds = data
        },

        setActiveAuction(state, data) {
            state.activeAuction = data
        },

        setActiveAuctionCount(state, data) {
            state.activeAuctionCount = data
        },

        setAllRounds(state, data) {
            state.allRoundsList = data
        },

        setSearchForm(state, data) {
            state.searchForm = data
        },

        setCurrentRoundId(state, data) {
            state.currentRoundId = data
        },

        setLotsCount(state, data) {
            state.lotsCount = data
        },

        setLotBids(state, data) {
            state.lotBids = data;
            state.lotBidsState = true;
        },

        setLotBidsState(state, value) {
            state.lotBidsState = value;
        },

        setLotItem(state, data) {
            state.lotItem = data;
        },

        createNewBid(state, data) {
            state.newBidConfig = data;
        },

        cancelNewBid(state, data = null) {
            state.newBidConfig = { bid:null, fn:null, lot:null, status:false};
        },

        setNewBidResponseEvent(state, data) {
            state.newBidResponseEvent = data;
        },

        setResponseInfo(state, data) {
            state.responseInfo = data
        },

        initResponseInfo(state) {
            let data = { status: false, errors: [], code: 0, message: '', action: '', data: []}
            state.responseInfo = data
        },
    },

    actions: {

        // Сделать ставку
        setBidAction(context, postData) {
            return Api.setNewBid(postData);
        },

        // Купить сейчас
        buyNowAction(context, postData) {
            return Api.buyNowBid(postData);
        },

        // Создать новый раунд аукциона
        addAuctionRound(context, roundItem) {
            context.commit('initResponseInfo');
            Api.addNewRound(roundItem).then(response => {
                const { save, result, error } = handle.saveParse(response)
                let message  = 'Не удалось создать новый раунд, попробуйте еще раз';
                let code = 201;
                if(save) {
                    code = 200;
                    message  = 'Новый раунд успешно создан';
                    context.dispatch('fetchActiveRounds');
                    context.dispatch('fetchAllRounds', { type: 0, limit: 0});
                }
                let respInfo = { code , message, status: true, action: 'add_round', data: response }
                context.commit('setResponseInfo', respInfo);
            }).catch(err => {
                const {code, data, response} = handle.errorParse(err);
                let errors   = data.errors;
                let message  = data.message;
                context.commit('setResponseInfo', { code, errors, message, action: 'add_round', status: true });
            });
        },

        // Добавить новые лоты
        createLots(context, lotForm) {
            context.commit('initResponseInfo');
            Api.addNewLots(lotForm).then(response => {
                // alert('Новые лоты успешно созданы');
                const { save, result, error } = handle.saveParse(response)
                let message  = 'Не удалось добавить лоты, попробуйте еще раз';
                let code = 201;
                if(save) {
                    code = 200;
                    let resMessage = '<br>Создано лотов: ' + result.save_count + ' | Всего отправлено:' +result.vins_count
                    message  = "Новые лоты успешно созданы" + resMessage;
                    context.dispatch('fetchAllRounds', { type: 0, limit: 0});
                }
                lotForm = {};
                let respInfo = { code , message, status: true, action: 'add_lots', data: response }
                context.commit('setResponseInfo', respInfo);
                return true;
            }).catch(err => {
                const {code, data, response} = handle.errorParse(err);
                let errors   = data.errors;
                let message  = data.message;
                context.commit('setResponseInfo', { code, errors, message, action: 'add_lots', status: true });
                return false;
            });
        },

        // Получить активные раунды
        fetchActiveRounds(context, auctionType = 1) {

            Api.getActiveRounds(auctionType).then(response => {
                const result = response.result;
                if(result[0] && result[0]['id']) {
                    result[0]['is_active'] = true;
                    let roundId = result[0]['id'];
                    context.dispatch('fetchActiveAuction', roundId);
                }
                context.commit('setActiveRounds', result);

            }).catch(error => {
                console.log(error);
            });
        },

        // Получить все раунды
        fetchAllRounds(context, params) {
            const auctionType = params.type;
            const limit = (params.limit) ? params.limit : 0;
            Api.getAllRounds([auctionType, limit]).then(response => {
                context.commit('setAllRounds', response.result);
            }).catch(error => {
                console.log(error);
            });
        },

        // Получить активный аукцион
        fetchActiveAuction(context, roundId = 0) {

            if(!roundId) roundId = context.state.currentRoundId;
            else         context.commit('setCurrentRoundId', roundId);
            const formData = context.state.searchForm;

            Api.getActiveAuction(formData, roundId).then(response => {
                const result    = response.result
                let auctionList = result.list;
                let lotsCount   = result.counts;
                context.commit('setActiveAuction', auctionList);
                context.commit('setLotsCount', lotsCount);
            }).catch(error => {
                console.log(error);
            });
        },

        // Получить количество лотов активного аукциона
        fetchActiveAuctionCout(context, roundId ) {

            if(!roundId) roundId = context.state.currentRoundId;
            else         context.commit('setCurrentRoundId', roundId);
            const formData = context.state.searchForm;

            Api.getActiveAuctionCount(formData, roundId).then(response => {
                context.commit('setActiveAuctionCount', response.result);
            }).catch(error => {
                console.log(error);
            });
        },

        // Получить ставки по лоту
        fetchLotBids(context, lotId) {
            Api.getBidsForLot([lotId]).then(response => {
                context.commit('setLotBids', response.result);
            }).catch(error => {
                console.log(error);
            });
        },

        // Получить лот
        fetchLotItem(context, lotId) {
            Api.getLot([lotId]).then(response => {
                context.commit('setLotItem', response.result);
            }).catch(error => {
                console.log(error);
            });
        },
    },

    getters: {
        getActiveRounds: state => {
            return state.activeRounds;
        },

        getActiveAuction: state => {
            return state.activeAuction;
        },

        getActiveAuctionCount: state => {
            return state.activeAuctionCount;
        },

        getAuctionTypes: (state) => {
            return state.auctionTypes;
        },

        getAllRounds: (state) => {
            return state.allRoundsList;
        },

        getSearchForm: (state) => {
            return state.searchForm;
        },

        getCurrentRoundId: (state) => {
            return state.currentRoundId;
        },

        getLotsCount:(state) => {
            return state.lotsCount
        },

        getLotStatuses: (state) => {
            return state.lotStatuses
        },

        getLotBids: (state) => {
            return state.lotBids
        },

        getLotBidsState: (state) => {
            return state.lotBidsState
        },

        getLotItem: (state) => {
            return state.lotItem
        },

        getNewBid: (state) => {
            return state.newBidConfig
        },

        getNewBidResponseEvent: (state) => {
            return state.newBidResponseEvent
        },

        getResponseInfo: (state) => {
            return state.responseInfo;
        },

    },

}

export default Auction

import Api from "@/api/api.endpoints";

const User = {

    state: {
        userId : 0,
        user   : {},
        users  : [],
        userFiles: [],
        errors : [],
    },

    mutations: {
    },

    actions: {
    },

    getters: {
    },

}

export default User

import {mapGetters, mapMutations} from 'vuex';
import apiClient from "@/api/api.endpoints";
import BaseLayout from "@/components/BaseLayout";
import ModalWrapper from "@/components/ModalWrapper";
import AdminLayout from "@/components/admin/AdminLayout";
import UniversalModal from "@/components/UniversalModal";
import AlertInfoMessage from "@/components/AlertInfoMessage";
import InputText from '@/components/form/InputText';

// var API_URL = apiUrl;
var uploadsUrl = UPLOADS_URL;

const Plugins = {
    install(Vue) {

        Vue.component('InputText', InputText)
        Vue.component('ModalWrapper', ModalWrapper)
        Vue.component('BaseLayout', BaseLayout)
        Vue.component('AdminLayout', AdminLayout)
        Vue.component('UniversalModal', UniversalModal)
        Vue.component('AlertInfoMessage', AlertInfoMessage)

        Vue.mixin({

            data() {
                return {
                    apiClient,
                    apiUrl,
                    uploadsUrl,
                    assetsUrl: '/assets',
                    curProfileId: 1,
                    currentUser: {},
                    isModalShow: false,
                }
            },

            computed: {
                ...mapGetters(['getActiveAuction']),
                getUploadsDir() {
                    return this.uploadsUrl;
                },
            },

            methods: {

                ...mapMutations(['setNewBidResponseEvent']),

                // Действия после успешной ставки пользователя
                newSetBidBrodcastEvent(emitResp) {

                    const newBid   = emitResp.bid;
                    const funcName = emitResp.fn;
                    const lot      = emitResp.lot;
                    const newBidPrice = newBid.bid_price;

                    const auction = this.getActiveAuction;

                    for(let i in auction) {
                        let item = auction[i];
                        if(item.lot_id == lot.lot_id) {

                            if(item.last_bid) {
                                item.last_bid.price = newBidPrice;
                            } else {
                                let lastBid = { price: newBidPrice}
                                item['last_bid'] = lastBid
                            }

                            const newBidEvent = {
                                status: true,
                                lot: item,
                                bid: newBid,
                                fn: funcName,
                            }

                            if(funcName == 'buyNow') {
                                delete auction[i];
                                let cardId = 'listing-lot-card-' + item.lot_id;
                                let lotCard = document.querySelector('#' + cardId);
                                lotCard.style.display = "none"
                            }

                            this.setNewBidResponseEvent(newBidEvent);

                            setTimeout(() => {
                                const newBidEventClose = {status: false, lot: {}, bid: {},}
                                this.setNewBidResponseEvent(newBidEventClose);
                            }, 6000);

                            return true
                        }
                    }
                },

                createSearchForm() {
                    return  {
                        page: 1,
                        limit: 6,
                        mark_id: 0,
                        model_id: 0,
                        price_from: '',
                        price_to: '',
                        vin: '',
                        lot_id: '',
                        city_id: '',
                        nds: -1,
                    };
                },

                showModal () {
                    this.isModalShow = true;
                },

                closeModal () {
                    this.isModalShow = false;
                },

                setTimer(fn, timer = 3000) {
                    setTimeout(fn, timer)
                },

                updateElemClassActive(className, activeClass, elem) {
                    let list = document.querySelectorAll(className);
                    for (let i in list) {
                        if (!list[i].style) continue;
                        let item = list[i];
                        item.classList.remove(activeClass)
                    }
                    elem.classList.add(activeClass);
                },

                htmlElemsRender(elems, fn = null) {
                    let results = []
                    for (let i in elems) {
                        let elem = elems[i]
                        if (fn) {
                            let res = fn(elem, i)
                            if (res == -1) return true
                            results.push(res)
                        }
                    }
                    return results
                },

                setToken(token) {
                    const tokenName = 'X-JWT-TOKEN';
                    localStorage.setItem(tokenName, token)
                },

                getToken() {
                    const tokenName = 'X-JWT-TOKEN';
                    return localStorage.getItem(tokenName)
                },

                deleteToken() {
                    const tokenName = 'X-JWT-TOKEN';
                    localStorage.removeItem(tokenName)
                },

                store(key, value = null) {
                    if (!value) return localStorage.getItem(key)
                    localStorage.setItem(key, value)
                },

                storeRemove(key) {
                    localStorage.removeItem(key)
                },

            } // --- Methods
        }) // --- Mixin

    }
}

export default Plugins

import ApiService from "@/api/api.service";
const http = new ApiService();

const WebsocketClient = {

    data() { return {

        socketHost: websocketHost,
        socketFolder: websocketFolder,
        socketPort: websocketPort,

        socket: null,
        responseData: [],
        clientsList: [],
        connectMessage: '',
        closeMessage: '',
        usersOnline: [],
        fromMessage: '',

    }},

    computed: {

        getUsersOnline() {
            return this.usersOnline;
        },

        usersOnlineCount() {
            let usersCount = 0;
            for(let i in this.usersOnline) {
                usersCount++;
            }
            return usersCount;
        },

    },

    created() {
        // this.websocketInit();
    },

    methods : {

        websocketClose() {
            this.socket = null;
        },

        sendToLocalTcpServer(data, fn = null) {
            const serverUrl = '/local_tcp_server.php';
            const realApiUrl = http.getApiUrl();
            const newApiUrl = 'http://' + this.socketHost +'/'+ this.socketFolder;
            http.setApiUrl(newApiUrl);
            http.post(serverUrl, data).then(response => {
                http.setApiUrl(realApiUrl);
                if(fn) fn(response);
            })
        },

        websocketInit() {

            let userId = this.getUserId;
            let userName = this.getUserName;
            const args = "user=" + userId;
            const port = this.socketPort;
            const wsServerName = this.socketHost;

            this.socket = new WebSocket ("ws://" +wsServerName+ ":" +port+ "/?" + args);

            // Подключение
            this.socket.onopen = (event) => {
                let message = "Успешное соединение";
                this.connectMessage = message;
                let user = {
                     user_id : this.getUserId,
                     username: this.getUserName,
                }
                this.sendSocketMessage({ type: 'add_user', data: user});
            }

            // Получаем сообщение
            this.socket.onmessage = (event) => {
                // console.log(event.data);
                let data = this.jsonParse(event.data);
                this.getSocketMessageHandle(data);
            }

            // Закрыть соединение
            this.socket.onclose = (event) => {
                this.socketClose(event);
            };

            // Ошибка
            this.socket.onerror = (event) => {
                this.socketError(event);
            };
        },

        // Отправляем сообщение
        sendSocketMessage(data) {
            let result = this.jsonToString(data);
            this.socket.send(result);
        },

        socketError(event) {
            this.closeMessage = 'Ошибка Websocket';
            console.log(this.closeMessage);
            console.log(event);
        },

        socketClose(event) {
            this.closeMessage = "Соединение закрыто";
            console.log(this.closeMessage);
            console.log(event);
        },

        setBid(type, message) {
            let data = { type, message }
            this.sendMessage(data);
        },

        jsonParse(data = null) {
            if(!data) return false;
            let result = JSON.parse(data);
            return result
        },

        jsonToString(data) {
            return JSON.stringify(data)
        },

        getSocketMessageHandle(data) {
            let type = data.type;
            switch(type) {
                case 'users_online' :
                    this.setUserOnline(data);
                    break;

                case 'from_user_message' :  // получаем сообщение от пользователя
                    this.fromUserMessageShow(data)
                    break;
            }
        },

        setUserOnline(data) {
            this.usersOnline = data.data;
        },

        // получаем сообщение от пользователя и отображаем
        fromUserMessageShow(data) {
            this.fromMessage = data.data;
            let userId       = data.from;
            if(this.getUsersOnline[userId]) {
                let userName = this.getUsersOnline[userId]['username'];
                let text     = this.fromMessage;
                let message = `<div>Вам пришло сообщение от ${userName}</div>
                                       <div>${text}</div>`;
                this.setAlertInfo({ message, timer: 5000 })
            }
        },

    },
}

export default WebsocketClient;
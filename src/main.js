import { createApp } from 'vue'
import { createPinia } from 'pinia';
import app from './App.vue'
import router from './router'
import store from './store'
import plugins from './plugins/Plugins'

const App = createApp(app);

App.use(plugins);
App.use(createPinia());
App.use(store);
App.use(router);
App.mount('#app');



import {reactive, ref } from 'vue';
import ApiService from "@/api/api.service";
const http = new ApiService();

const userModel = {
    created_at: '',
    updated_at: '',
    active: '',
    address: '',
    sex: '',
    email: '',
    login: '',
    note: '',
    phone: '',
    photo: '',
    role: 0,
    user_id: 0,
    age: '',
    username: '',
}

const curUserModel = Object.assign({}, userModel)
// const register = Object.assign({}, userModel)

const currentUser  = reactive(curUserModel);
const userRegister = reactive({
    role: 0,
    email: '',
    login: '',
    phone: '',
    username: '',
})

const tokenName     = ref('jwt_token');
const storeUserFieldName = ref('current_user');
const authStatus    = ref(false);

export default function useAuth() {

    const token = ref('');
    const forgotPwdState = ref(false);
    const preloader = reactive({
        state: false,
        message: '',
    })

    const authData = reactive({
        email    : 'dzion67@mail.ru',
        password : '1234',
    });

    const respInfo = reactive({
        message : '',
        color   : '',
        state   : false,
    });

    // Работа с local store
    const setLocalStore = (key, value) => {
        return localStorage.setItem(key, value)
    }

    const getLocalStore = (key) => {
        return localStorage.getItem(key);
    }

    const removeLocalStore = (key) => {
        return localStorage.removeItem(key);
    }
    // ./ Работа с local store

    // Войти
    const login = () => {
        logout();
        preloader.state = true;
        respInfo.state = false;
        let url = '/post/auth/login';
        const postData = authData;
        http.post(url, postData).then((response) => {
            preloader.state = false;
            loginResponse(response);
        });
    }

    const loginResponse = (response) => {
        let message = '';
        respInfo.state = true;
        if (!response || !response.status || !response.token) {
            message = `Не удалось авторизоваться, неправильный логин или пароль
                       <br> попробуйте еще раз`;
            if(response.err_message)
                message = response.err_message

            respInfo.color   = 'red';
            respInfo.message = message;
            return false
        }

        for(let name in currentUser) {
            if(response.user[name]) {
                currentUser[name] = response.user[name]
            }
        }

        token.value = response.token;
        authStatus.value = true;

        setLocalStore(tokenName.value, token.value);
        setLocalStore(storeUserFieldName.value,
                      JSON.stringify(currentUser));
        authData.email = '';
        authData.password = '';

        respInfo.color   = 'green';
        respInfo.message = 'Успешная авторизация';

    }
    // ./  Войти


    // Разлогинивание
    const logout = () => {
        token.value      = '';
        preloader.state  = true;
        authStatus.value = false;

        respInfo.state   = false;
        respInfo.message = '';
        respInfo.color   = '';

        for(let name in currentUser) {
             currentUser[name] = '';
        }
        removeLocalStore(tokenName.value);
        removeLocalStore(storeUserFieldName.value);
        preloader.state = false;

    }

    // Регистрация пользователя
    const register = () => {
        logout();
        preloader.state = true;
        const url = '/post/user/register'
        const postData = userRegister;
        http.post(url, postData).then((response) => {
            preloader.state = false;
            registerResponse(response);
        });
    }

    const registerResponse = (response) => {
        let message = '';
        let color = '';
        if (response.save_result || response.result || response.status) {
            message = `Новый пользователь успешно создан`;
            color = 'green';
        } else {
            message = `Не удалось выполнить регистрацию`;
            color = 'red';
        }

        respInfo.state   = true;
        respInfo.color   = color
        respInfo.message = message;

        for(let name in userRegister) {
            userRegister[name] = '';
        }
    }
    // ./ Регистрация пользователя

    // Получаем активного пользователя из local store
    const getStoreCurrentUser = () => {
        let user = getLocalStore(storeUserFieldName.value);
        if(user) user = JSON.parse(user)
        return user
    }

    // Получаем активного пользователя из local store
    const getStoreJwtToken = () => {
        let token = getLocalStore(tokenName.value);
        if(!token) token = '';
        return token
    }

    // Сбросить пароль
    const forgotYouPassword = () => {
        logout();
        respInfo.state = false;
        const apiUrl = '/user/forgot-password/' + authData.email
        http.get(apiUrl).then(response => {
            forgotResponse(response)
        })
    }

    const forgotResponse = (response) => {
        let message = 'Не удалось сбросить пароль, попробуйте еще раз'
        let color   = 'red'
        if (response.save_result) {
            message = `На почту отправлено письмо с новым паролем
                     <br/> (После входа необходимо изменить пароль)
                     <br/> Если письмо не пришло, посмотрите в папке 'Спам'`;
            color = 'green';
            forgotPwdState.value = false;
        }

        respInfo.state = true;
        respInfo.color   = color
        respInfo.message = message
    }
    // ./ Сбросить пароль

    return {
        login,
        logout,
        getStoreCurrentUser,
        getStoreJwtToken,
        //----------
        forgotYouPassword,
        forgotPwdState,
        //----------
        register,
        userRegister,
        //----------
        currentUser,
        authData,
        respInfo,
        authStatus,
        token,
        preloader,
    }
}
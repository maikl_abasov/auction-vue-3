import { reactive, ref } from 'vue';

export default function useValidate() {

    const inputStyleUpdate = (event, res) => {
        let target = event.target;
        let border = '1px green solid';
        let message = '';
        if(res.error) {
            message = res.message;
            border = '1px red solid';
        }
        target.style.border = border;
        target.nextElementSibling.innerHTML = message;
    }

    const validate = (value, rules) => {

        let separator = "|";
        const rulesList = rules.split(separator);
        const response = (error, message, rule) => { return { error, message, rule } };

        for(let i in rulesList) {
            let ruleItem  = rulesList[i];
            let itemArr   = ruleItem.split(':');
            let rule      = itemArr[0];
            let message   = '';
            let ruleParam = '';
            switch (rule) {
                case 'required' :
                    message = 'Обязательное поле';
                    if(!parseInt(value.length)) {
                        return response( true, message, rule);
                    }
                    break;

                case 'min-len' :
                    ruleParam = itemArr[1];
                    message = `Должно быть не меньше ${ruleParam} символов`;
                    if(parseInt(value.length) < parseInt(ruleParam)) {
                        return response( true, message, rule);
                    }
                    break;
            }
        }

        return response( false, '', '');
    }

    return {
        validate,
        inputStyleUpdate,
        formatPhoneNumber,
    }

    function formatPhoneNumber(number)  {
        let arr = number.split('');

        let index = 0;
        for(let num in arr) {
            let val = arr[num];
            let simState = false;
            if(num == index && val != '(' && val != ' ') {
                arr[num] = ' (' + val;
                simState = true;
            }

            if(num == (index + 3) && val != ')' && val != ' ') {
                arr[num] = ') ' + val;
                simState = true;
            }

            if(num == (index + 3 + 3) && val != '-') {
                arr[num] = '-' + val;
                simState = true;
            }

            if(num == (index + 3 + 3 + 2)  && val != '-') {
                arr[num] = '-' + val;
                simState = true;
            }

            if(num > (index + 3 + 3 + 2 + 1)) {
                delete arr[num];
            }
        }

        let newValue = "+7 " + arr.join('');
        return newValue;
    }
}